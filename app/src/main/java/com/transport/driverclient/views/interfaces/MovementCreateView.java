package com.transport.driverclient.views.interfaces;

import com.transport.driverclient.data.models.Driver;
import com.transport.driverclient.data.models.Route;

import java.util.List;

public interface MovementCreateView {
    void startDriverCreateView();

    void startRouteCreateView();

    void setRoutes(List<Route> routes);

    void setDrivers(List<Driver> drivers);

    Driver getDriverSelected();

    Route getRouteSelected();
}
