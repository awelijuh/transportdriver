package com.transport.driverclient.views.interfaces;

import com.transport.driverclient.data.models.Stop;

import java.util.List;

public interface RouteCreateView {
    String getName();

    void closeActivity();

    Stop getStopSelected();

    void setStops(List<Stop> stops);
}
