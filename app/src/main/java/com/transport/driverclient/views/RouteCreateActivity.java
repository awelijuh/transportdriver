package com.transport.driverclient.views;

import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.transport.driverclient.R;
import com.transport.driverclient.adapters.StopAdapter;
import com.transport.driverclient.data.models.Stop;
import com.transport.driverclient.presenters.RouteCreatePresenter;
import com.transport.driverclient.views.interfaces.RouteCreateView;

import java.util.List;

public class RouteCreateActivity extends AppCompatActivity implements RouteCreateView {

    private RouteCreatePresenter presenter;
    private StopAdapter stopAdapter;

    private EditText mName;
    private RecyclerView recyclerView;
    private Spinner stopSpinner;
    private Button routeStopAddButton;

    private Button routeCreateButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_route_create);

        mName = findViewById(R.id.routeNameEditText);
        recyclerView = findViewById(R.id.routeStopRecyclerView);
        stopSpinner = findViewById(R.id.routeStopSpinner);
        routeStopAddButton = findViewById(R.id.routeStopAddButton);
        routeCreateButton = findViewById(R.id.saveRouteButton);

        stopAdapter = new StopAdapter(this);
        stopSpinner.setAdapter(stopAdapter);

        presenter = new RouteCreatePresenter(this);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(presenter.getRouteStopAdapter());


        routeStopAddButton.setOnClickListener(v -> presenter.addRouteStop());
        routeCreateButton.setOnClickListener(v -> presenter.createRoute());

        presenter.loadStops();
    }

    @Override
    public String getName() {
        return mName.getText().toString();
    }

    @Override
    public void closeActivity() {
        finish();
    }

    @Override
    public Stop getStopSelected() {
        return stopAdapter.getItem(stopSpinner.getSelectedItemPosition());
    }

    @Override
    public void setStops(List<Stop> stops) {
        stopAdapter.clear();
        stopAdapter.addAll(stops);
        stopAdapter.notifyDataSetChanged();
    }

}
