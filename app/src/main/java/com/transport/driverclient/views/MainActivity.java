package com.transport.driverclient.views;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Spinner;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.transport.driverclient.R;
import com.transport.driverclient.adapters.DriverAdapter;
import com.transport.driverclient.adapters.RouteSpinnerArrayAdapter;
import com.transport.driverclient.data.models.Driver;
import com.transport.driverclient.data.models.Route;
import com.transport.driverclient.presenters.MovementCreatePresenter;
import com.transport.driverclient.views.interfaces.MovementCreateView;

import java.util.List;

public class MainActivity extends AppCompatActivity implements MovementCreateView {

    private MovementCreatePresenter presenter;

    private Spinner driverSpinner;

    private DriverAdapter driverAdapter;
    private RouteSpinnerArrayAdapter routeAdapter;


    private Spinner routeSpinner;


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        driverSpinner = findViewById(R.id.driverSpinner);
        routeSpinner = findViewById(R.id.routeSpinner);
        afterView();
    }

    @Override
    protected void onResume() {
        super.onResume();
        presenter.load();
    }

    void afterView() {
        presenter = new MovementCreatePresenter(this);
        driverAdapter = new DriverAdapter(this);
        routeAdapter = new RouteSpinnerArrayAdapter(this);

        driverSpinner.setAdapter(driverAdapter);
        routeSpinner.setAdapter(routeAdapter);

        findViewById(R.id.createDriverButton).setOnClickListener(this::createDriverClick);
        findViewById(R.id.createRouteButton).setOnClickListener(v -> presenter.routeCreate());
        findViewById(R.id.movementCreateButton).setOnClickListener(v -> presenter.movementSave());
        presenter.load();
    }


    private void createDriverClick(View view) {
        presenter.createDriverClick();
    }


    @Override
    public void startDriverCreateView() {
        Intent intent = new Intent(this, DriverCreateActivity.class);
        startActivity(intent);
    }

    @Override
    public void startRouteCreateView() {
        Intent intent = new Intent(this, RouteCreateActivity.class);
        startActivity(intent);
    }


    @Override
    public void setRoutes(List<Route> routes) {
        routeAdapter.clear();
        if (routes != null) {
            routeAdapter.addAll(routes);
        }
        routeAdapter.notifyDataSetChanged();
    }

    @Override
    public void setDrivers(List<Driver> drivers) {
        driverAdapter.clear();
        if (drivers != null) {
            driverAdapter.addAll(drivers);
        }
        driverAdapter.notifyDataSetChanged();
    }

    @Override
    public Driver getDriverSelected() {
        return driverAdapter.getItem(driverSpinner.getSelectedItemPosition());
    }

    @Override
    public Route getRouteSelected() {
        return routeAdapter.getItem(routeSpinner.getSelectedItemPosition());
    }


}
