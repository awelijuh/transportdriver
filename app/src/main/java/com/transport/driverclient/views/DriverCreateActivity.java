package com.transport.driverclient.views;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import androidx.appcompat.app.AppCompatActivity;

import com.transport.driverclient.R;
import com.transport.driverclient.presenters.DriverCreatePresenter;
import com.transport.driverclient.views.interfaces.DriverCreateView;


public class DriverCreateActivity extends AppCompatActivity implements DriverCreateView {


    DriverCreatePresenter driverCreatePresenter;


    private EditText mEmail;

    private EditText mName;

    private EditText mPassword;

    private Button saveButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_driver_create);

        mEmail = findViewById(R.id.driverEmailEditText);
        mName = findViewById(R.id.driverNameEditText);
        mPassword = findViewById(R.id.driverPasswordEditText);
        saveButton = findViewById(R.id.driverSaveButton);
        driverCreatePresenter = new DriverCreatePresenter(this);
        saveButton.setOnClickListener(this::saveClick);
    }

    private void saveClick(View view) {
        driverCreatePresenter.create();
    }

    @Override
    public void closeActivity() {
        finish();
    }


    @Override
    public String getName() {
        return mName.getText().toString();
    }

    @Override
    public String getEmail() {
        return mEmail.getText().toString();
    }

    @Override
    public String getPassword() {
        return mPassword.getText().toString();
    }
}
