package com.transport.driverclient.views.interfaces;

public interface DriverCreateView {

    void closeActivity();

    String getName();

    String getEmail();

    String getPassword();

}
