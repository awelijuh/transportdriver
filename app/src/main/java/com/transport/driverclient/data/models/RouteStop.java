package com.transport.driverclient.data.models;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class RouteStop {

    private Long routeId;

    private Long stopId;

    private Long pos;

    private Long id;

}
