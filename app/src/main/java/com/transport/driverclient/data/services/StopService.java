package com.transport.driverclient.data.services;



import com.transport.driverclient.data.models.Stop;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

public interface StopService {
    @GET("stop")
    Call<List<Stop>> getAll();
}
