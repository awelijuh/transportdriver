package com.transport.driverclient.data.models;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class Stop {

    private String name;

    private String key;

    private LatLng latLng;

    private Long id;

}
