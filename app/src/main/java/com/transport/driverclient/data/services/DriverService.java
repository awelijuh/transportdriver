package com.transport.driverclient.data.services;

import com.transport.driverclient.data.models.Driver;
import com.transport.driverclient.data.models.Request;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;


public interface DriverService {
    @POST("driver")
    Call<Driver> save(@Body Driver driver);

    @GET("driver")
    Call<List<Driver>> getAll();
}
