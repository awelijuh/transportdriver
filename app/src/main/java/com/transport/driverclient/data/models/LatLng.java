package com.transport.driverclient.data.models;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class LatLng {

    Double latitude;

    Double longitude;

}
