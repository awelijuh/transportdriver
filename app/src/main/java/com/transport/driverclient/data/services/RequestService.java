package com.transport.driverclient.data.services;



import com.transport.driverclient.data.models.Request;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface RequestService {
    @POST("request")
    Call<Request> save(@Body Request request);
}
