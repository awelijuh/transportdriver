package com.transport.driverclient.data.services;

import com.transport.driverclient.data.models.Movement;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface MovementService {

    @POST("movement")
    Call<Movement> create(@Body Movement movement);
}
