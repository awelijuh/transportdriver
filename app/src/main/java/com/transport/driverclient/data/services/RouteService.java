package com.transport.driverclient.data.services;



import com.transport.driverclient.data.models.Route;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;

public interface RouteService {
    @GET("route")
    Call<List<Route>> getAll();

    @POST("route")
    Call<Route> create(@Body Route route);
}
