package com.transport.driverclient.data.models;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class Request {
    private Long stopId;

    private Long destinationStopId;

    private Long routeId;

    private String status;

    private String creationTime;
}
