package com.transport.driverclient.data.models;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class Movement {

    private Long driverId;

    private Long routeId;

    private Long transportId;


    private Long id;

}
