package com.transport.driverclient.data.models;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class Driver {

    private Long id;

    private String name;

    private String email;

    private String password;

}
