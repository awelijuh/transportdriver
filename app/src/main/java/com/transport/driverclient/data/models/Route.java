package com.transport.driverclient.data.models;

import java.util.List;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class Route {

    private String name;

    private Long id;

    private List<Long> stops;

}
