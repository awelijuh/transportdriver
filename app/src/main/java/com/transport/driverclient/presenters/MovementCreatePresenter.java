package com.transport.driverclient.presenters;

import android.content.Context;
import android.widget.Toast;

import com.transport.driverclient.data.models.Driver;
import com.transport.driverclient.data.models.Movement;
import com.transport.driverclient.data.models.Route;
import com.transport.driverclient.data.services.DriverService;
import com.transport.driverclient.data.services.MovementService;
import com.transport.driverclient.data.services.RouteService;
import com.transport.driverclient.views.interfaces.MovementCreateView;

import java.util.List;

import lombok.RequiredArgsConstructor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.transport.driverclient.data.ServiceGenerator.createService;

@RequiredArgsConstructor
public class MovementCreatePresenter {

    private final MovementCreateView view;

    private final MovementService movementService = createService(MovementService.class);
    private final RouteService routeService = createService(RouteService.class);
    private final DriverService driverService = createService(DriverService.class);


    public void createDriverClick() {
        view.startDriverCreateView();
    }

    public void load() {
        routeService.getAll().enqueue(new Callback<List<Route>>() {
            @Override
            public void onResponse(Call<List<Route>> call, Response<List<Route>> response) {
                if (response.isSuccessful()) {
                    view.setRoutes(response.body());
                }
            }

            @Override
            public void onFailure(Call<List<Route>> call, Throwable t) {

            }
        });

        driverService.getAll().enqueue(new Callback<List<Driver>>() {
            @Override
            public void onResponse(Call<List<Driver>> call, Response<List<Driver>> response) {
                if (response.isSuccessful()) {
                    view.setDrivers(response.body());
                }
            }

            @Override
            public void onFailure(Call<List<Driver>> call, Throwable t) {

            }
        });
    }

    public void movementSave() {
        movementService.create(
                Movement.builder()
                        .driverId(view.getDriverSelected().getId())
                        .routeId(view.getRouteSelected().getId())
                        .build()
        ).enqueue(new Callback<Movement>() {
            @Override
            public void onResponse(Call<Movement> call, Response<Movement> response) {
                if (response.isSuccessful()) {
                    Toast.makeText(((Context) view).getApplicationContext(), "Ок!", Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<Movement> call, Throwable t) {

            }
        });
    }

    public void routeCreate() {
        view.startRouteCreateView();
    }
}
