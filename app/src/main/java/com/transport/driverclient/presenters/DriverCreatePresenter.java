package com.transport.driverclient.presenters;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.transport.driverclient.data.models.Driver;
import com.transport.driverclient.data.services.DriverService;
import com.transport.driverclient.views.interfaces.DriverCreateView;

import lombok.RequiredArgsConstructor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.transport.driverclient.data.ServiceGenerator.createService;

@RequiredArgsConstructor
public class DriverCreatePresenter {

    private final DriverCreateView driverCreateView;
    private final DriverService driverService = createService(DriverService.class);

    public void create() {
        driverService.save(
                Driver.builder()
                        .name(driverCreateView.getName())
                        .email(driverCreateView.getEmail())
                        .password(driverCreateView.getPassword())
                        .build()
        ).enqueue(new Callback<Driver>() {
            @Override
            public void onResponse(Call<Driver> call, Response<Driver> response) {
                if (response.isSuccessful()) {
                    Toast.makeText(((Context) driverCreateView).getApplicationContext(), "Ок!", Toast.LENGTH_SHORT).show();
                    driverCreateView.closeActivity();
                }
                Log.d("request-server", "" + response.code());
            }

            @Override
            public void onFailure(Call<Driver> call, Throwable t) {
                Log.d("request-server", "" + t.getMessage());
            }
        });
    }

}
