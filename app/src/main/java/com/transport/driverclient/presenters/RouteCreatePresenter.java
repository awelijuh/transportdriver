package com.transport.driverclient.presenters;

import android.content.Context;
import android.widget.Toast;

import com.transport.driverclient.adapters.RouteStopAdapter;
import com.transport.driverclient.data.models.Route;
import com.transport.driverclient.data.models.Stop;
import com.transport.driverclient.data.services.RouteService;
import com.transport.driverclient.data.services.StopService;
import com.transport.driverclient.views.interfaces.RouteCreateView;

import java.util.ArrayList;
import java.util.List;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.transport.driverclient.data.ServiceGenerator.createService;

@RequiredArgsConstructor
public class RouteCreatePresenter {

    private final RouteCreateView view;
    @Getter
    private final RouteStopAdapter routeStopAdapter = new RouteStopAdapter();

    private final StopService stopService = createService(StopService.class);
    private final RouteService routeService = createService(RouteService.class);

    public void createRoute() {
        List<Long> stops = new ArrayList<>();
        for (Stop stop : routeStopAdapter.getStops()) {
            stops.add(stop.getId());
        }
        routeService.create(
                Route.builder()
                        .name(view.getName())
                        .stops(stops)
                        .build())
                .enqueue(new Callback<Route>() {
                    @Override
                    public void onResponse(Call<Route> call, Response<Route> response) {
                        if (response.isSuccessful()) {
                            Toast.makeText(((Context) view).getApplicationContext(), "Ок!", Toast.LENGTH_SHORT).show();
                            view.closeActivity();
                        }
                    }

                    @Override
                    public void onFailure(Call<Route> call, Throwable t) {

                    }
                });
    }


    public void addRouteStop() {
        routeStopAdapter.addItem(view.getStopSelected());
    }

    public void loadStops() {
        stopService.getAll().enqueue(new Callback<List<Stop>>() {
            @Override
            public void onResponse(Call<List<Stop>> call, Response<List<Stop>> response) {
                if (response.isSuccessful()) {
                    view.setStops(response.body());
                }
            }

            @Override
            public void onFailure(Call<List<Stop>> call, Throwable t) {

            }
        });
    }
}
