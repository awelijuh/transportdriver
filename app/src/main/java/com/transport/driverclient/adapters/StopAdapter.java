package com.transport.driverclient.adapters;

import android.content.Context;

import androidx.annotation.NonNull;

import com.transport.driverclient.data.models.Stop;

public class StopAdapter extends AbstractSpinnerArrayAdapter<Stop> {
    public StopAdapter(@NonNull Context context) {
        super(context);
    }

    @Override
    protected String getName(Stop item) {
        return item.getName();
    }
}
