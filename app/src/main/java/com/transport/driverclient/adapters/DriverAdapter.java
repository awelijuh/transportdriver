package com.transport.driverclient.adapters;

import android.content.Context;

import androidx.annotation.NonNull;

import com.transport.driverclient.data.models.Driver;

public class DriverAdapter extends AbstractSpinnerArrayAdapter<Driver> {
    public DriverAdapter(@NonNull Context context) {
        super(context);
    }

    @Override
    protected String getName(Driver item) {
        return item.getName();
    }
}
