package com.transport.driverclient.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.transport.driverclient.R;

public abstract class AbstractSpinnerArrayAdapter<T> extends ArrayAdapter<T> {


    public AbstractSpinnerArrayAdapter(@NonNull Context context) {
        super(context, R.layout.spinner_item);
    }

    @Override
    public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        return getView(position, convertView, parent);
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) getContext()
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.spinner_item, parent, false);


        TextView textView = view.findViewById(R.id.text1);
        textView.setText(this.getName(getItem(position)));

        return view;
    }

    protected abstract String getName(T item);

}
