package com.transport.driverclient.adapters;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.transport.driverclient.R;
import com.transport.driverclient.data.models.Stop;

import java.util.ArrayList;
import java.util.List;

import lombok.Getter;

public class RouteStopAdapter extends RecyclerView.Adapter<RouteStopAdapter.RouteStopViewHolder> {

    @Getter
    private List<Stop> stops = new ArrayList<>();

    @NonNull
    @Override
    public RouteStopViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.spinner_item, parent, false);
        return new RouteStopViewHolder(view);
    }

    public void clear() {
        stops.clear();
        notifyDataSetChanged();
    }

    public void addItem(Stop stop) {
        stops.add(stop);
        Log.d("adapter", stop.getName());
        notifyDataSetChanged();
    }

    @Override
    public void onAttachedToRecyclerView(@NonNull RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    @Override
    public void onBindViewHolder(@NonNull RouteStopViewHolder holder, int position) {
        holder.textView.setText(stops.get(position).getName());
    }

    @Override
    public int getItemCount() {
        return stops.size();
    }

    static class RouteStopViewHolder extends RecyclerView.ViewHolder {

        final TextView textView;

        public RouteStopViewHolder(@NonNull View itemView) {
            super(itemView);
            textView = itemView.findViewById(R.id.text1);
        }
    }
}
