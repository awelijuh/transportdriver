package com.transport.driverclient.adapters;

import android.content.Context;

import androidx.annotation.NonNull;

import com.transport.driverclient.data.models.Route;

public class RouteSpinnerArrayAdapter extends AbstractSpinnerArrayAdapter<Route> {
    public RouteSpinnerArrayAdapter(@NonNull Context context) {
        super(context);
    }

    @Override
    protected String getName(Route item) {
        return item.getName();
    }
}
